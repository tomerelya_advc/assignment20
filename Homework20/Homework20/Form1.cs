﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;




namespace Homework20
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            add_Cards(this,null);
            Thread control = new Thread(controlFunc);
            control.Start();
        }

        private void add_Cards(object sender, EventArgs e)
        {
            string path = "";
            string temp = "";
            int x = 31;
            int y = 335;
            Random rnd = new Random();
            int lot = 0;
            int lot1 = 0;
            Point nextPoint = new Point(x, y);
            Point bluePoint = new Point(0,0);
            for (int i = 0;i<=10;i++)
            {
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "dynPic" + i;
                currentPic.Image = global::Homework20.Properties.Resources.card_back_red;
                currentPic.Location = nextPoint;
                if (i == 5)
                {
                    bluePoint.X = nextPoint.X;
                    bluePoint.Y = nextPoint.Y - 325;
                }
                currentPic.Size = new System.Drawing.Size(81,114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                currentPic.Click += delegate(object sender1, EventArgs e1)
                {
                    string currentIndex = currentPic.Name.Substring(currentPic.Name.Length - 1);
                    MessageBox.Show("You have clicked card #" + currentIndex);
                    lot = rnd.Next(2, 9);
                    temp += lot;
                    temp += "_of_";
                    lot = rnd.Next(1, 9);
                    if (lot == 1)
                        temp += "hearts";
                    else if (lot == 2)
                        temp += "spades";
                    else if (lot == 3)
                        temp += "diamonds";
                    else if (lot == 4)
                        temp += "clubs";
                    else if (lot == 5)
                    {
                        temp = "ace_of_";
                        lot1 = rnd.Next(1, 5);
                        if (lot1 == 1)
                            temp += "hearts";
                        else if (lot1 == 2)
                            temp += "spades";
                        else if (lot1 == 3)
                            temp += "diamonds";
                        else if (lot1 == 4)
                            temp += "clubs";
                        else if (lot1 == 5)
                            temp += "spades2";
                    }
                    else if(lot ==6)
                    {
                        temp = "10_of_";
                        lot1 = rnd.Next(1, 4);
                        if (lot1 == 1)
                            temp += "hearts";
                        else if (lot1 == 2)
                            temp += "spades";
                        else if (lot1 == 3)
                            temp += "diamonds";
                        else if (lot1 == 4)
                            temp += "clubs";
                    }
                    else if (lot == 7)
                    {
                        temp = "jack_of_";
                        lot1 = rnd.Next(1, 5);
                        if (lot1 == 1)
                            temp += "hearts2";
                        else if (lot1 == 2)
                            temp += "spades2";
                        else if (lot1 == 3)
                            temp += "diamonds2";
                        else if (lot1 == 4)
                            temp += "clubs2";
                    }
                    else if (lot == 8)
                    {
                        temp = "queen_of_";
                        lot1 = rnd.Next(1, 5);
                        if (lot1 == 1)
                            temp += "hearts2";
                        else if (lot1 == 2)
                            temp += "spades2";
                        else if (lot1 == 3)
                            temp += "diamonds2";
                        else if (lot1 == 4)
                            temp += "clubs2";
                    }
                    else if (lot == 9)
                    {
                        temp = "king_of_";
                        lot1 = rnd.Next(1, 5);
                        if (lot1 == 1)
                            temp += "hearts2";
                        else if (lot1 == 2)
                            temp += "spades2";
                        else if (lot1 == 3)
                            temp += "diamonds2";
                        else if (lot1 == 4)
                            temp += "clubs2";
                    }
                    //currentPic.Image = Image.FromFile()
                    textBox1.Text = temp;
                    path += "C:\\Users\\User\\Documents\\Visual Studio 2013\\Projects\\Homework20\\Homework20\\Resources\\";
                    path += temp;
                    path += ".png";
                    currentPic.Image = Image.FromFile(path);
                    path = "";
                    temp = "";

                };
                this.Controls.Add(currentPic);
                nextPoint.X = nextPoint.X + 87;
                if(nextPoint.X > this.Size.Width)
                {
                    nextPoint.X = x;
                    nextPoint.Y = currentPic.Size.Height + 10;
                }
            }
            System.Windows.Forms.PictureBox blueCard = new PictureBox();
            blueCard.Name = "blueCard";
            blueCard.Image = global::Homework20.Properties.Resources.card_back_blue;
            blueCard.Location = bluePoint;
            blueCard.Size = new System.Drawing.Size(81, 114);
            blueCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Controls.Add(blueCard);
        }

        private void controlFunc()
        {
            string str;
            int bytes;
            Socket s = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp);

            Invoke((MethodInvoker)delegate { textBox1.Text = "Establishing Connection..."; });
            s.Connect("127.0.0.1", 8820);
            Invoke((MethodInvoker)delegate { textBox1.Text = "Connection Established..."; });
            do
            {
                byte[] bufferIn = new byte[4];
                bytes = s.Receive(bufferIn, 0, 4);
                str = new ASCIIEncoding().GetString(bufferIn);
                  
            }
            while (bytes > 0);
            Invoke((MethodInvoker)delegate { textBox1.Text = str; });


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
